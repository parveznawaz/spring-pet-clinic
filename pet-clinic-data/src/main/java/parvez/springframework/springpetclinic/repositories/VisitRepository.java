package parvez.springframework.springpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import parvez.springframework.springpetclinic.model.Visit;

public interface VisitRepository extends CrudRepository<Visit,Long> {
}
