package parvez.springframework.springpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import parvez.springframework.springpetclinic.model.Speciality;

public interface SpecialtyRepository extends CrudRepository<Speciality,Long> {
}
