package parvez.springframework.springpetclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import parvez.springframework.springpetclinic.model.Vet;

public interface VetRepository extends CrudRepository<Vet,Long> {
}
