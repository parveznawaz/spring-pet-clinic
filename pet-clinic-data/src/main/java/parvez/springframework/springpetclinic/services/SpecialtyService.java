package parvez.springframework.springpetclinic.services;

import parvez.springframework.springpetclinic.model.Speciality;

public interface SpecialtyService extends CrudService<Speciality,Long> {

}
