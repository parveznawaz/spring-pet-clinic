package parvez.springframework.springpetclinic.services;

import parvez.springframework.springpetclinic.model.PetType;

public interface PetTypeService extends CrudService<PetType,Long> {
}
