package parvez.springframework.springpetclinic.services;

import parvez.springframework.springpetclinic.model.Pet;

public interface PetService extends CrudService<Pet, Long> {

}
