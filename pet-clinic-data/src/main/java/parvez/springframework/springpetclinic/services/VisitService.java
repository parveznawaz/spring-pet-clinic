package parvez.springframework.springpetclinic.services;

import parvez.springframework.springpetclinic.model.Visit;

public interface VisitService extends CrudService<Visit,Long>{
}
