package parvez.springframework.springpetclinic.services;

import parvez.springframework.springpetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
