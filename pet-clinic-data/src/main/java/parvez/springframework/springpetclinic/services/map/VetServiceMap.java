package parvez.springframework.springpetclinic.services.map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import parvez.springframework.springpetclinic.model.Speciality;
import parvez.springframework.springpetclinic.model.Vet;
import parvez.springframework.springpetclinic.services.SpecialtyService;
import parvez.springframework.springpetclinic.services.VetService;

import java.util.Set;

@Service
@Profile({"default","map"})
public class VetServiceMap extends AbstractMapService<Vet, Long> implements VetService {
    private final SpecialtyService specialtyService;

    public VetServiceMap(SpecialtyService specialtyService) {
        this.specialtyService = specialtyService;
    }

    @Override
    public Set<Vet> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Vet object) {
        super.delete(object);
    }

    @Override
    public Vet save(Vet object) {

//        if (object.getSpecialties().size() > 0){
//            object.getSpecialties().forEach(speciality -> {
//                if(speciality.getId() == null){
//                    Speciality savedSpecialty = specialtyService.save(speciality);
//                    speciality.setId(savedSpecialty.getId());
//                    speciality.setId(savedSpecialty.getId());
//                }
//            });
//        }

        return super.save(object);
    }

    @Override
    public Vet findById(Long id) {
        return super.findById(id);
    }
}
